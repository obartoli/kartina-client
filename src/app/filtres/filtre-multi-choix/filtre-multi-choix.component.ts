import { Component, OnInit, Input } from '@angular/core';
import { Themes } from 'src/app/shared/themes';

@Component({
  selector: 'app-filtre-multi-choix',
  templateUrl: './filtre-multi-choix.component.html',
  styleUrls: ['./filtre-multi-choix.component.scss']
})
export class FiltreMultiChoixComponent implements OnInit {

  @Input() titre: String;
  @Input() lstFiltres: Array<String> = new Array<String>();

  constructor() { }

  ngOnInit() {
  }

}
