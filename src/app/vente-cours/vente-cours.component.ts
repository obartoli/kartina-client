import { Component, OnInit } from '@angular/core';
import { Utilisateur } from '../shared/utilisateur';
import { Pageable } from '../shared/pagination/pageable';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { UtilisateurService } from '../service/utilisateur.service';
import { PhotoService } from '../service/photo.service';
import { Vente } from '../shared/vente';

@Component({
  selector: 'app-vente-cours',
  templateUrl: './vente-cours.component.html',
  styleUrls: ['./vente-cours.component.scss']
})
export class VenteCoursComponent implements OnInit {
  utilisateur: Utilisateur;
  lstVente: Array<Vente> = new Array<Vente>();
  utilisateurId: number;
  pageable: Pageable;

  constructor(private route: ActivatedRoute, private utilisateurServ: UtilisateurService, private photoServ: PhotoService) {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.utilisateurId = parseInt(params.get('id'));
      console.log("ID sélectionné", this.utilisateurId);
      this.utilisateurServ.getUser(this.utilisateurId).subscribe(response => {
        console.log("ID sélectionné", this.utilisateurId);
        this.utilisateur = response as Utilisateur;
        console.log("artiste", this.utilisateur);
      });
       
      this.photoServ.getVenteCours(this.utilisateurId).subscribe(

        response => {
          this.pageable = response as Pageable;
          this.lstVente = this.pageable.content; {
            console.log("Liste vente en cours", this.lstVente);
      }
    });
  });
}

  /*getPage(page?: number) {
    this.photoServ.getVenteCours(this.utilisateurId, page).subscribe(response => {
      this.lstVente = response; {
        console.log("Liste vente en cours", this.lstVente);
      }
    });
  }*/

  ngOnInit() {
  }

}
