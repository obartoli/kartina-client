import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VenteCoursComponent } from './vente-cours.component';

describe('VenteCoursComponent', () => {
  let component: VenteCoursComponent;
  let fixture: ComponentFixture<VenteCoursComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VenteCoursComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VenteCoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
