import {
  Component,
  OnInit
} from '@angular/core';
import {
  Vente
} from '../shared/vente';
import {
  Photo
} from '../shared/photo';
import {
  PhotoService
} from '../service/photo.service';
import {
  Router,
  ActivatedRoute,
  ParamMap,
  NavigationStart
} from '@angular/router';
import {
  AuthentificationService
} from '../authentification/authentification.service';
import {
  UtilisateurService
} from '../service/utilisateur.service';
import {
  Utilisateur
} from '../shared/utilisateur';
import {
  ThemesService
} from '../service/themes.service';
import {
  Themes
} from '../shared/themes';
import {
  FormatService
} from '../service/format.service';
import {
  Format
} from '../shared/format';
import {
  isUndefined
} from 'util';
import {
  UploadFileService
} from '../service/upload-file.service';

@Component({
  selector: 'app-nouvelle-vente',
  templateUrl: './nouvelle-vente.component.html',
  styleUrls: ['./nouvelle-vente.component.scss']
})
export class NouvelleVenteComponent implements OnInit {
  vente: Vente = new Vente();
  photographie: Photo = new Photo();
  uploadPhoto: Photo;
  utilisateurId: number;
  utilisateur: Utilisateur;
  format: Format;
  lstTheme = Array < Themes > ();
  lstFormat = Array < Format > ();
  private lstCheckFormat: Array < boolean > = new Array < boolean > ();
  private lstCheckTheme: Array < boolean > = new Array < boolean > ();
  selectedFiles: FileList
  currentFileUpload: File
  progress: {
    percentage: number
  } = {
    percentage: 0
  }

  constructor(private route: ActivatedRoute, 
              private themeServ: ThemesService, 
              private utilisateurServ: UtilisateurService,
              private formatServ: FormatService, 
              private photoServ: PhotoService, 
              private uploadService: UploadFileService, 
              private router: Router, 
              private authService: AuthentificationService) {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let email = this.authService.getSubject();
      this.utilisateurServ.getMonCompte(email).subscribe(response => {
        this.utilisateur = response as Utilisateur;
        console.log("vendeur", this.utilisateur);
        this.utilisateurId = this.utilisateur.utilisateurId;
        console.log("ID sélectionné", this.utilisateurId);
      });
      this.themeServ.getAll().subscribe(response => {
        this.lstTheme = response as Themes[];
        console.log("theme", this.lstTheme);
      });
      this.formatServ.getAllFormat().subscribe(response => {
        this.lstFormat = response as Format[];
        console.log("format", this.lstFormat);
      });
      this.vente.photographie = this.photographie;

      this.photographie.formats = new Array < Format > ();
      this.photographie.themes = new Array < Themes > ();
      //Premier du boolean est true, mais si meilleure manière, le faire
      this.lstCheckFormat[0] = true;
    });
  }

  ngOnInit() {}

  ajoutVente() {
    this.lstCheckFormat.forEach((element, index) => {
      if (element) {
        this.photographie.formats.push(this.lstFormat[index]);
      }
    });
    this.lstCheckTheme.forEach((element, index) => {
      if (element) {
        this.photographie.themes.push(this.lstTheme[index]);
      }
    });
    this.vente.utilisateur = this.utilisateur;
    this.vente.photographie.orientation = this.uploadPhoto.orientation;
    this.vente.photographie.urlImage = this.uploadPhoto.urlImage;
    console.log(this.vente);

    if(this.isThemeRequired()) {
      this.photoServ.nvleVente(this.vente).subscribe(
        () => {
          this.router.navigateByUrl('/venteencours/' + this.utilisateur.utilisateurId);
          console.log("vente:", this.vente);
        }
      );
    } else {
      alert("Un thème doit être sélectionné au minimum !");
    }
  }

  selectFile(event) {
    const file = event.target.files.item(0)

    if (file.type.match('image.*')) {
      this.selectedFiles = event.target.files;
    } else {
      alert('invalid format!');
    }
  }

  upload() {
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedFiles.item(0)
    this.uploadService.pushFileToStorage(this.currentFileUpload).subscribe(response => {
      this.uploadPhoto = response as Photo;
      this.progress.percentage = 100;
      console.log("photo uploadée: ", this.uploadPhoto);

      /*if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        console.log('Fichier téléchargé');
      }*/
    })
    this.selectedFiles = undefined
  }

  checkFormat(index: number) {
    if (isUndefined(this.lstCheckFormat[index])) {
      this.lstCheckFormat[index] = true;
    } else {
      this.lstCheckFormat[index] = !this.lstCheckFormat[index];
    }
    console.log("index sélectionné", this.lstCheckFormat);
  }

  checkTheme(index: number) {
    if (isUndefined(this.lstCheckTheme[index])) {
      this.lstCheckTheme[index] = true;
    } else {
      this.lstCheckTheme[index] = !this.lstCheckTheme[index];
    }
    console.log("index thème sélectionné", this.lstCheckTheme);
  }

  isThemeRequired() : boolean {
    let bool = false;
    this.lstCheckTheme.forEach((element) => {
      if(element)
        bool = true;
    });
    return bool;
  }

}
