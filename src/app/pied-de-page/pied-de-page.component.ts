import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pied-de-page',
  templateUrl: './pied-de-page.component.html',
  styleUrls: ['./pied-de-page.component.scss']
})
export class PiedDePageComponent implements OnInit {

  pathLogoTweet: String = "assets/img/icons8-twitter-50.png";

  pathLogoFacebook: String = "assets/img/icons8-facebook-50.png"


  constructor() { }

  ngOnInit() {
  }

}
