import { Component, OnInit , Input} from '@angular/core';
import { Utilisateur } from '../shared/utilisateur';
import { Vente } from '../shared/vente';

@Component({
  selector: 'app-detail-photo',
  templateUrl: './detail-photo.component.html',
  styleUrls: ['./detail-photo.component.scss']
})
export class DetailPhotoComponent implements OnInit {
  @Input() vente: Vente;
  @Input() bool: boolean;
  /*photo: Photo = this.vente.photographie;*/
  constructor() {
   }

  ngOnInit() {
    
  }

}
