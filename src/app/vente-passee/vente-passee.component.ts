import { Component, OnInit } from '@angular/core';
import { Utilisateur } from '../shared/utilisateur';
import { Pageable } from '../shared/pagination/pageable';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { UtilisateurService } from '../service/utilisateur.service';
import { PhotoService } from '../service/photo.service';
import { Vente } from '../shared/vente';

@Component({
  selector: 'app-vente-passee',
  templateUrl: './vente-passee.component.html',
  styleUrls: ['./vente-passee.component.scss']
})
export class VentePasseeComponent implements OnInit {
  pageable: Pageable;
  utilisateur: Utilisateur;
  lstVente: Array<Vente> = new Array<Vente>();
  utilisateurId: number;
  
  constructor(private route: ActivatedRoute, private utilisateurServ: UtilisateurService, private photoServ: PhotoService) {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.utilisateurId = parseInt(params.get('id'));
      console.log("ID sélectionné", this.utilisateurId);
      this.utilisateurServ.getUser(this.utilisateurId).subscribe(response => {
        console.log("ID sélectionné", this.utilisateurId);
        this.utilisateur = response as Utilisateur;
        console.log("artiste", this.utilisateur);
      });
      this.photoServ.getVentePassee(this.utilisateurId).subscribe(response => {
        this.pageable = response as Pageable;
        this.lstVente = this.pageable.content; {
          console.log("Liste vente passée", this.lstVente);
        }
      });
    });
  }

  /*getPage(page?: number) {
    this.photoServ.getVentePassee(this.utilisateurId, page).subscribe(response => {
      this.lstVente = response as Pageable; {
        console.log("Liste vente passée", this.lstVente);
      }
    });
  }*/
  
  ngOnInit() {
  }

}
