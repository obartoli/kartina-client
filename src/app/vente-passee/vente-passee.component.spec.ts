import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VentePasseeComponent } from './vente-passee.component';

describe('VentePasseeComponent', () => {
  let component: VentePasseeComponent;
  let fixture: ComponentFixture<VentePasseeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VentePasseeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VentePasseeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
