import {
  Component,
  OnInit
} from '@angular/core';
import {
  PhotoService
} from '../service/photo.service';
import {
  Vente
} from '../shared/vente';
import {
  Pageable
} from '../shared/pagination/pageable';
import {
  ThemesService
} from '../service/themes.service';
import {
  Themes
} from '../shared/themes';
import {
  isUndefined
} from 'util';
import {
  ActivatedRoute,
  ParamMap
} from '@angular/router';
import {
  FormControl,
  Form
} from '@angular/forms';
import {
  FormatService
} from '../service/format.service';
import {
  Format
} from '../shared/format';

@Component({
  selector: 'app-parcours-catalogue',
  templateUrl: './parcours-catalogue.component.html',
  styleUrls: ['./parcours-catalogue.component.scss']
})
export class ParcoursCatalogueComponent implements OnInit {

  //Thème
  private listeTheme: Array < Themes > ;
  private themeSelected: number; //index du thème sélectionné

  //Orientations
  private lstOrientations: Array < string > ;
  private lstCheckOrientations: Array < boolean > = new Array < boolean > (); //liste des états (check ou non) des orientations

  //Formats
  private lstFormats: Array < string > ;
  private lstCheckFormats: Array < boolean > = new Array < boolean > (); //liste des états (check ou non) des formats

  //Dernières ventes
  private isLastCopiesSelected: boolean = false;

  // Les photos à afficher
  private pageable: Pageable = new Pageable();

  private pageTitle: string = "Photographies";
  private pageDescription: string = "Nouveautés";

  constructor(private route: ActivatedRoute,
    private photoService: PhotoService,
    private themeService: ThemesService,
    private formatService: FormatService) {

    this.route.paramMap.subscribe((params: ParamMap) => {
      let theme = params.get("theme");

      this.themeService.getAll().subscribe(
        response => {
          this.listeTheme = response as Themes[];
          if (theme) {
            this.listeTheme.forEach((element, index) => {
              if (element.theme.match(theme)) {
                this.pageTitle = element.theme;
                this.pageDescription = element.description;
                this.themeSelected = index;
              }
            });
          }
          this.sellsByCriteria();
        }
      );

      this.photoService.getOrientations().subscribe(
        response => {
          this.lstOrientations = response as Array < string > ;
        }
      );

      this.formatService.getFormats().subscribe(
        response => {
          this.lstFormats = response as Array < string > ;
        }
      );
    });
  }

  ngOnInit() {}

  sellsByCriteria(page ? : number) {

    //Création de l'url pour récupérer les photos en cours de vente en fonction des filtres sélectionnés [Thèmes / Formats / Orientations]
    let criteriaUrl: string = "";

    if (!isUndefined(this.themeSelected)) {
      criteriaUrl += "themes=" + this.listeTheme[this.themeSelected].theme;
    }
    
    criteriaUrl = this.appendUrl(this.lstCheckOrientations, this.lstOrientations, "orientations", criteriaUrl);
    criteriaUrl = this.appendUrl(this.lstCheckFormats, this.lstFormats, "formats", criteriaUrl);

    if (page) {
      if (criteriaUrl.length > 0)
        criteriaUrl += '&';
      criteriaUrl += "page=" + page;
    }

    if (criteriaUrl.length > 0) {
      criteriaUrl = '?' + criteriaUrl;
    }

    this.photoService.getAllVente(criteriaUrl).subscribe(
      response => {
        this.pageable = response as Pageable;
      }
    );
  }

  //On vérifie si l'élément à été sélectionné, si c'est le cas on le rajoute dans l'url...
  private appendUrl(lstCheck: boolean[], lstType: string[], type: string, url: string): string {

    let isWritten: boolean = false;
    lstCheck.forEach((element, index: number) => {
      if (element) {
        //Si la 'famille' de l'élément n'a pas encore été ajoutée
        if (!isWritten) {

          //Si on a encore aucun paramètre dans l'url alors on ajoute '&' avant le type, sinon on ajoute uniquement le type [ex: '&formats=...]
          if (url.length > 0)
            url += "&" + type + "=";
          else
            url += type + "=";

          isWritten = true;

        //Si la 'famille' de l'élément a déjà été ajouté, on rajoute l'élément à la suite après une virugle [ex: &formats=grand,géant...]
        } else {
          url += ",";
        }
        url += lstType[index];
      }
    });
    return url;
  }

  lastCopies(page ? : number) {
    this.photoService.getLastCopies(page).subscribe(
      response => {
        this.pageable = response as Pageable;
      }
    )
  }

  checkOrientations(index: number) {

    if (isUndefined(this.lstCheckOrientations[index])) {
      this.lstCheckOrientations[index] = true;
    } else {
      this.lstCheckOrientations[index] = !this.lstCheckOrientations[index];
    }
    this.sellsByCriteria();
  }

  checkFormats(index: number) {

    if (isUndefined(this.lstCheckFormats[index])) {
      this.lstCheckFormats[index] = true;
    } else {
      this.lstCheckFormats[index] = !this.lstCheckFormats[index];
    }
    this.sellsByCriteria();
  }
}
