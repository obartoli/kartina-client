import { Component, OnInit } from '@angular/core';
import { PhotoService } from '../service/photo.service';
import { Vente } from '../shared/vente';
import { Pageable } from '../shared/pagination/pageable';
import { Utilisateur } from '../shared/utilisateur';
import { Photo } from '../shared/photo';
import { UtilisateurService } from '../service/utilisateur.service';

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent implements OnInit {
  
  private randomNewSell: Vente;
  private randomTravelSell: Vente;
  private randomBWSell: Vente;
  private randomUserSell: Vente;
  private randomLastCopies: Vente;

  private titre1 = "Nouvelles oeuvres";
  private titre2 = "Paysage";
  private titre3 = "Noir & Blanc";
  private titre4 = "Artiste";
  private titre5 = "Derniers exemplaires";

  
  private lien1 = "/catalogue";
  private lien2 = "/catalogue/Paysage";
  private lien3 = "/catalogue/Noir et blanc";
  private lien4 = "/artiste/2";
  private lien5 = "/catalogue";

  private descLien1 = "Parcourir la nouvelle collection";
  private descLien2 = "Parcourir la collection";
  private descLien3 = "Découvrir la collection";
  private descLien4 = "Découvrir l'artiste";
  private descLien5 = "Découvrir les oeuvres";
  
  
  private pageable: Pageable;

  constructor(private photoServ: PhotoService, private utilisateurServ: UtilisateurService) { 
    
    //Section Nouvelles oeuvres
    this.photoServ.getRandomNewVente().subscribe(response => {
      this.randomNewSell = response as Vente;
      console.log("random vente", this.randomNewSell);
    });

    //Section 6 Meilleures ventes
    this.photoServ.getBestVente().subscribe(response => {
      this.pageable = response as Pageable;
      console.log(this.pageable);
      
    });

    //Section Paysage -- id = 2 // TODO : réfléchir à une méthode plus dynamique
    this.photoServ.getRandomThemeVente(2).subscribe(response => {
      this.randomTravelSell = response as Vente;
    });

    //Section Noir & blanc -- id = 1 // TODO : réfléchir à une méthode plus dynamique
    this.photoServ.getRandomThemeVente(1).subscribe(response => {
      this.randomBWSell = response as Vente;
    });

    //Section Artiste -- id = 2  l'utilisateur 2 correspond à un vendeur chez moi [olivier] // TODO réfléchir à une méthode plus dynamique
    this.photoServ.getRandomUserVente(2).subscribe(response => {
      this.randomUserSell = response as Vente;
      this.titre4 = this.randomUserSell.utilisateur.nom + " " + this.randomUserSell.utilisateur.prenom;
    });

    //Section Derniers exemplaires
    this.photoServ.getRandomLastCopies().subscribe(response => {
      this.randomLastCopies = response as Vente;
    });

  }

  ngOnInit() {
  }

}
