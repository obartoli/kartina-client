import {
  Component,
  OnInit
} from '@angular/core';
import {
  Commande
} from '../shared/commande';
import {
  StatutCommande
} from '../shared/statut-commande';
import {
  LigneCommande
} from '../shared/ligne-commande';
import {
  Photo
} from '../shared/photo';
import {
  Vente
} from '../shared/vente';
import {
  Utilisateur
} from '../shared/utilisateur';
import {
  Format
} from '../shared/format';
import {
  Finition
} from '../shared/finition';
import {
  Cadre
} from '../shared/cadre';
import {
  Router
} from '@angular/router';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit {

  private commande: Commande = new Commande();
  private txTva: number = 0.2;
  private tva: number;

  constructor(private router: Router) {
    this.commande = JSON.parse(localStorage.getItem("commande"));
    if (this.commande) {
      this.updatePrice();
    }
  }

  ngOnInit() {}

  public deleteLine(index: number) {
    this.commande.ligneCommandes.splice(index, 1);

    if (this.commande.ligneCommandes.length == 0) {
      localStorage.removeItem("commande");
      this.commande = undefined;
    }

    this.updatePrice();
  }

  public updatePrice() {

    if (this.commande) {
      console.log("change quantité");
      this.commande.prixTtc = 0;
      this.tva = 0;

      this.commande.ligneCommandes.forEach(element => {
        if (element.quantite < 1)
          element.quantite = 1;

        element.prixUnitaire = element.vente.photographie.prixPhoto *
          element.format.majoration *
          element.finition.majoration *
          (element.cadre?element.cadre.majorationCadre:1);

        this.tva += element.prixUnitaire * element.quantite * this.txTva;

        element.prixUnitaire=element.prixUnitaire * (1 + this.txTva);
        this.commande.prixTtc += element.prixUnitaire * element.quantite;
      });
    }
    console.log("change quantité");
    if(this.commande.ligneCommandes.length > 0)
      localStorage.setItem("commande", JSON.stringify(this.commande));
  }

  private goToPayement() {
    localStorage.setItem("commande", JSON.stringify(this.commande));
    this.router.navigateByUrl('/paiement');
  }
}
