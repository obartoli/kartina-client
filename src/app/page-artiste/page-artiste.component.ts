import {
  Component,
  OnInit,
  Input
} from '@angular/core';
import {
  Utilisateur
} from '../shared/utilisateur';
import {
  UtilisateurService
} from '../service/utilisateur.service';
import {
  Router,
  ActivatedRoute,
  ParamMap
} from '@angular/router';
import {
  Photo
} from '../shared/photo';
import {
  PhotoService
} from '../service/photo.service';
import { Vente } from '../shared/vente';
import { Pageable } from '../shared/pagination/pageable';

@Component({
  selector: 'app-page-artiste',
  templateUrl: './page-artiste.component.html',
  styleUrls: ['./page-artiste.component.scss']
})
export class PageArtisteComponent implements OnInit {
  utilisateur: Utilisateur;
  lstVente: Pageable;
  artiste: Utilisateur = new Utilisateur();
  utilisateurId: number;
  venteId: number;

  constructor(private router: Router, private route: ActivatedRoute, private utilisateurServ: UtilisateurService, private photoServ: PhotoService) {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.utilisateurId = parseInt(params.get('id'));
      console.log("ID sélectionné", this.utilisateurId);
      this.utilisateurServ.getUser(this.utilisateurId).subscribe(response => {
        console.log("ID sélectionné", this.utilisateurId);
        this.artiste = response as Utilisateur;
        console.log("artiste", this.artiste);
      });
      this.photoServ.getVenteByArtiste(this.utilisateurId).subscribe(response => {
        this.lstVente = response as Pageable; {
          console.log("ID vente sélectionné", this.venteId);
          console.log("Liste vente", this.lstVente);
        }
      });
    });
  }

  onBack(): void {
    this.router.navigate(['/artiste']);
  }

  getPage(page?: number) {
      this.photoServ.getVenteByArtiste(this.utilisateurId, page).subscribe(
        response => {
          this.lstVente = response as Pageable;
          console.log(this.lstVente);
        }
      );
  }

  goBack() {
    this.router.navigate(['/artiste']);
  }

  goPrevious() {
    let prev = this.utilisateurServ.lstArtiste.indexOf(this.artiste) - 1;
    this.router.navigate(['/artiste', this.getId(prev)]);
  }

  goNext() {
    let next = this.utilisateurServ.lstArtiste.indexOf(this.artiste) + 1;
    this.router.navigate(['/artiste', this.getId(next)]);
  }

  hasPrev() {
    return this.utilisateurServ.lstArtiste.indexOf(this.artiste) > 0;
  }

  hasNext() {
    return this.utilisateurServ.lstArtiste.indexOf(this.artiste) < this.utilisateurServ.lstArtiste.length - 1;
  }

  getId(index: number) {
    return this.utilisateurServ.lstArtiste[index].utilisateurId;
  }

  ngOnInit() {

  }

}
