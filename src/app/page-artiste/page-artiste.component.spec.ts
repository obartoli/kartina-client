import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageArtisteComponent } from './page-artiste.component';

describe('PageArtisteComponent', () => {
  let component: PageArtisteComponent;
  let fixture: ComponentFixture<PageArtisteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageArtisteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageArtisteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
