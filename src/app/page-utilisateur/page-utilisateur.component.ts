import {
  Component,
  OnInit,
  Input
} from '@angular/core';
import {
  UtilisateurService
} from '../service/utilisateur.service';
import {
  Utilisateur
} from '../shared/utilisateur';
import {
  AuthentificationService
} from '../authentification/authentification.service';

@Component({
  selector: 'app-page-utilisateur',
  templateUrl: './page-utilisateur.component.html',
  styleUrls: ['./page-utilisateur.component.scss']
})
export class PageUtilisateurComponent implements OnInit {
  @Input() utilisateur: Utilisateur;
  @Input() utilisateurId: number;
  utilisateurEmail: String;
  moncompte: Utilisateur = new Utilisateur();
  role: String;

  constructor(private authServ: AuthentificationService, private utilisateurServ: UtilisateurService) {

    this.utilisateurEmail = this.authServ.getSubject();
    this.utilisateurServ.getMonCompte(this.utilisateurEmail).subscribe(response => {
      this.moncompte.prenom = localStorage.getItem("prenom");
      this.moncompte = response as Utilisateur;
    });


    this.role = this.authServ.getAudience();
  }

  ngOnInit() {}

  deconnection() {
    this.authServ.logout();
  }
}
