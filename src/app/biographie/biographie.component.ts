import { Component, OnInit } from '@angular/core';
import { UtilisateurService } from '../service/utilisateur.service';
import { Utilisateur } from '../shared/utilisateur';
import { Adresse } from '../shared/adresse';
import { Router } from '@angular/router';
import { AuthentificationService } from '../authentification/authentification.service';

@Component({
  selector: 'app-biographie',
  templateUrl: './biographie.component.html',
  styleUrls: ['./biographie.component.scss']
})
export class BiographieComponent implements OnInit {
  private u = new Utilisateur();
  private ad = new Adresse();

  constructor(private utilisateurService: UtilisateurService, private router: Router,
    private authentificationService: AuthentificationService) {
    let email = this.authentificationService.getSubject();
    if (email) {
      this.utilisateurService.getMonCompte(email).subscribe(response => {
        console.log("email récupéré", email);
        this.u = response as Utilisateur;
        console.log("update", this.u);
      });
    }
  }

  ngOnInit() {
  }

  updateBio() {
    this.utilisateurService.update(this.u).subscribe(
      data => {
        this.router.navigateByUrl('/moncompte');
      }
    );
  }
}
