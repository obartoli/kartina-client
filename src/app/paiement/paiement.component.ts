import {
  Component,
  OnInit,
  ViewChild
} from '@angular/core';

import {
  FormGroup,
  FormBuilder,
  Validators
} from "@angular/forms";

import {
  Commande
} from '../shared/commande';

import {
  StripeService,
  Elements,
  Element as StripeElement,
  ElementsOptions
} from "ngx-stripe";
import {
  Utilisateur
} from '../shared/utilisateur';
import {
  AuthentificationService
} from '../authentification/authentification.service';
import {
  CommandeService
} from '../service/commande.service';
import {
  Adresse
} from '../shared/adresse';

@Component({
  selector: 'app-paiement',
  templateUrl: './paiement.component.html',
  styleUrls: ['./paiement.component.scss']
})
export class PaiementComponent implements OnInit {

  elements: Elements;
  card: StripeElement;

  stripeTest: FormGroup;

  private stripeResponse: string;

  private commande: Commande;
  private utilisateur: Utilisateur = new Utilisateur();
  private isConnected: boolean;
  private hasPayed: boolean = false;

  constructor(private fb: FormBuilder,
    private stripeService: StripeService,
    private authService: AuthentificationService,
    private commandeService: CommandeService) {

    this.isConnected = this.authService.isLoggedIn();

  }

  ngOnInit() {

    this.commande = JSON.parse(localStorage.getItem("commande"));
    if (this.commande)
      console.log(this.commande);

    this.stripeTest = this.fb.group({
      name: ['', [Validators.required]]
    });
    this.stripeService.elements()
      .subscribe(elements => {
        this.elements = elements;
        // Only mount the element the first time
        if (!this.card) {
          this.card = this.elements.create('card', {
            style: {
              base: {
                iconColor: '#666EE8',
                color: '#31325F',
                // lineHeight: '40px',
                fontWeight: 300,
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSize: '18px',
                '::placeholder': {
                  color: '#7F8790'
                  // color: '#CFD7E0'
                }
              }
            }
          });
          this.card.mount('#card-element');
        }
      });
  }

  buy() {
    this.commande.utilisateur = this.utilisateur;

    if (this.utilisateur && this.checkAdress()) {
      this.hasPayed = true;
      const name = this.utilisateur.nom + ' ' + this.utilisateur.prenom;
      this.stripeService.createToken(this.card, {
        name
      }).subscribe(
        result => {
          if (result.token) {
            // Use the token to create a charge or a customer
            // https://stripe.com/docs/charges
            this.stripeResponse = 'Transaction réussie !';
            console.log(result.token);

            this.commandeService.sendOrder(this.commande).subscribe(
              result => {
                console.log(result);
              });

          } else if (result.error) {
            // Error creating the token
            this.stripeResponse = '' + result.error.message;
            console.log(result.error.message);

            this.hasPayed = false;
          }
          alert(this.stripeResponse);
        }
      );
    } else {
      alert("Veuillez renseigner vos informations personnelles.");
    }
  }

  getUser(utilisateur: Utilisateur) {
    this.utilisateur = utilisateur;
  }

  private checkAdress() {
    let adresse: Adresse = this.utilisateur.adresses[0];

    return (adresse.codePostal && adresse.pays && adresse.rue && adresse.ville);
  }
}
