import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

import {
  Pageable
} from '../shared/pagination/pageable';
import { Vente } from '../shared/vente';

@Component({
  selector: 'app-liste-photo',
  templateUrl: './liste-photo.component.html',
  styleUrls: ['./liste-photo.component.scss']
})
export class ListePhotoComponent implements OnInit {
  @Input() pageable: Pageable;
  @Input() vente: Vente;
  @Input() bool: boolean;
  @Output() onPageChange = new EventEmitter < number > ();

  constructor() {
  }

  ngOnInit() { }

  //Envoie de la page à charger au composent parent
  getPage(page: number) {
    this.onPageChange.emit(page - 1);
  }

}
