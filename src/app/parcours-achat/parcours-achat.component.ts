import {
  Component,
  OnInit
} from '@angular/core';
import {
  ActivatedRoute,
  ParamMap
} from '@angular/router';
import {
  VenteService
} from '../service/vente.service';
import {
  Vente
} from '../shared/vente';
import {
  Format
} from '../shared/format';
import {
  Finition
} from '../shared/finition';
import {
  Cadre
} from '../shared/cadre';
import {
  Commande
} from '../shared/commande';
import {
  LigneCommande
} from '../shared/ligne-commande';


@Component({
  selector: 'app-parcours-achat',
  templateUrl: './parcours-achat.component.html',
  styleUrls: ['./parcours-achat.component.scss']
})
export class ParcoursAchatComponent implements OnInit {

  private vente: Vente;
  private venteId: number;
  private formats: Array < Format > ;
  private formatSelect: Format;
  private btnClass: Array < string > = ["btn btn-light btn-block"];
  private finitions: Array < Finition > ;
  private completed: boolean = false;
  private finitionSelect: Finition = new Finition();
  private btnClass2: Array < string > = ["btn btn-light btn-block"];
  private completed2: boolean = false;
  private cadres: Array < Cadre > ;
  private cadreSelect: Cadre;
  private btnClass3: Array < string > = ["btn btn-light btn-block"];
  private completed3: boolean = false;
  private commande: Commande;
  private ligneCommande: LigneCommande;
  private prixFormat: number;
  private prixFinition: number;
  private prixCadre: number;
  private afficheRoute: boolean = false;
  private prixTotal: number = 0;


  constructor(private route: ActivatedRoute, private _serviceVente: VenteService) {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.venteId = parseInt(params.get('id'));
      this._serviceVente.getVente(this.venteId).subscribe(
        data => {
          this.vente = data as Vente;
          this.formats = this.vente.photographie.formats;
          console.log("vente : ", this.vente);
          this.btnClass.length = this.formats.length;
          this.btnClass.fill("btn btn-light btn-block");

        }
      )
    });
  }
  ngOnInit() {}

  selectionnerFormat(i: number) {
    this.btnClass.fill("btn btn-light btn-block");
    this.btnClass[i] = 'btn btn-outline-success btn-block';
    this.formatSelect = this.formats[i];
    //console.log(this.formatSelect);
    this.completed = true;
    this.prixFormat = +((this.vente.photographie.prixPhoto * this.formats[i].majoration).toFixed(2));

    this.finitions = this.formatSelect.finitions;
    //console.log(this.finitions);
    this.btnClass2.length = this.finitions.length;
    this.btnClass2.fill("btn btn-light btn-block");
    this.completed2 = false;
    this.completed3 = false;
    this.afficheRoute = false;
    this.finitionSelect = new Finition();
  }

  selectionnerFinition(i: number) {
    this.cadreSelect = undefined;
    this.btnClass2.fill("btn btn-light btn-block");
    this.btnClass2[i] = 'btn btn-outline-success btn-block';
    this.finitionSelect = this.finitions[i];
    //console.log(this.finitionSelect);
    if (this.finitionSelect.finitionId == 3) {
      this.completed2 = false
    } else {
      this.completed2 = true;
    }
    this.prixFinition = +((this.prixFormat * this.finitions[i].majoration).toFixed(2));



    this.cadres = this.finitionSelect.cadres;
    //console.log(this.cadres);
    this.btnClass3.length = this.cadres.length;
    this.btnClass3.fill("btn btn-light btn-block");

    this.completed3 = false;
    this.afficheRoute = false;
  }

  selectionnerCadre(i: number) {
    this.btnClass3.fill("btn btn-light btn-block");
    this.btnClass3[i] = 'btn btn-outline-success btn-block';
    this.cadreSelect = this.cadres[i];
    //console.log(this.cadreSelect);
    this.completed3 = true;

    this.prixCadre = +((this.prixFinition * this.cadres[i].majorationCadre).toFixed(2));
    this.afficheRoute = false;
  }

  ajoutPanier() {
    this.commande = JSON.parse(localStorage.getItem("commande"));
    if (!this.commande) {
      this.commande = new Commande();
      this.commande.ligneCommandes = new Array < LigneCommande > ();
    }

    this.ligneCommande = new LigneCommande();
    this.commande.ligneCommandes.push(this.ligneCommande);
    this.ligneCommande.vente = this.vente;
    this.ligneCommande.format = this.formatSelect;
    this.ligneCommande.finition = this.finitionSelect;
    this.ligneCommande.cadre = this.cadreSelect;
    this.ligneCommande.quantite = 1;
    localStorage.setItem("commande", JSON.stringify(this.commande));
    this.afficheRoute = true;
    if (this.completed3 == true) {
      this.prixTotal = this.prixCadre * (1.2);
    } else {
      this.prixTotal = this.prixFinition * (1.2);
    }
  }
}
