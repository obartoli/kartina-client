import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParcoursAchatComponent } from './parcours-achat.component';

describe('ParcoursAchatComponent', () => {
  let component: ParcoursAchatComponent;
  let fixture: ComponentFixture<ParcoursAchatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParcoursAchatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParcoursAchatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
