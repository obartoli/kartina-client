import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MatStepperModule } from '@angular/material/stepper';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatIconModule } from '@angular/material';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPaginationModule } from 'ngx-pagination';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { NgxStripeModule } from 'ngx-stripe';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SaisieNouvelUtilisateurComponent } from './saisie-nouvel-utilisateur/saisie-nouvel-utilisateur.component';
import { EnteteComponent } from './entete/entete.component';
import { FilArianeComponent } from './fil-ariane/fil-ariane.component';
import { PiedDePageComponent } from './pied-de-page/pied-de-page.component';
import { MentionsLegalesComponent } from './mentions-legales/mentions-legales.component';
import { ConditionsGeneralesUtilisationComponent } from './conditions-generales-utilisation/conditions-generales-utilisation.component';
import { AccueilComponent } from './accueil/accueil.component';
import { ConnexionInscriptionComponent } from './connexion-inscription/connexion-inscription.component';
import { AuthentificationService } from './authentification/authentification.service';
import { AuthentificationIntercepteur } from './authentification/authentification-intercepteur';
import { DetailPhotoComponent } from './detail-photo/detail-photo.component';
import { ListePhotoComponent } from './liste-photo/liste-photo.component';
import { PhotoService } from './service/photo.service';
import { UtilisateurService } from './service/utilisateur.service';
import { SectionQuatroComponent } from './section-quatro/section-quatro.component';
import { ArtisteComponent } from './artiste/artiste.component';
import { ParcoursCatalogueComponent } from './parcours-catalogue/parcours-catalogue.component';
import { FiltreMultiChoixComponent } from './filtres/filtre-multi-choix/filtre-multi-choix.component';
import { PageArtisteComponent } from './page-artiste/page-artiste.component';
import { PageUtilisateurComponent } from './page-utilisateur/page-utilisateur.component';
import { ParcoursAchatComponent } from './parcours-achat/parcours-achat.component';
import { PanierComponent } from './panier/panier.component';
import { BiographieComponent } from './biographie/biographie.component';
import { NouvelleVenteComponent } from './nouvelle-vente/nouvelle-vente.component';
import { VenteCoursComponent } from './vente-cours/vente-cours.component';
import { VentePasseeComponent } from './vente-passee/vente-passee.component';
import { PaiementComponent } from './paiement/paiement.component';
import { AideComponent } from './aide/aide.component';
import { UploadFileService } from './service/upload-file.service';
import { DetailUploadComponent } from './detail-upload/detail-upload.component';
import { FormUploadComponent } from './form-upload/form-upload.component';
import { HistoriqueUrlService } from './service/historique-url.service';
import { CommandeService } from './service/commande.service';
import { FormatService } from './service/format.service';
import { VenteService } from './service/vente.service';
import { ThemesService } from './service/themes.service';

@NgModule({
  declarations: [
    AppComponent,
    SaisieNouvelUtilisateurComponent,
    EnteteComponent,
    FilArianeComponent,
    PiedDePageComponent,
    MentionsLegalesComponent,
    ConditionsGeneralesUtilisationComponent,
    AccueilComponent,
    ConnexionInscriptionComponent,
    DetailPhotoComponent,
    ListePhotoComponent,
    SectionQuatroComponent,
    ArtisteComponent,
    ParcoursCatalogueComponent,
    FiltreMultiChoixComponent,
    PageArtisteComponent,
    PageUtilisateurComponent,
    ParcoursAchatComponent,
    PanierComponent,
    BiographieComponent,
    NouvelleVenteComponent,
    VenteCoursComponent,
    VentePasseeComponent,
    PaiementComponent,
    AideComponent,
    DetailUploadComponent,
    FormUploadComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgxPaginationModule,
    MatStepperModule,
    BrowserAnimationsModule,
    MatIconModule,
    NgbModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    NgxStripeModule.forRoot('pk_test_TYooMQauvdEDq54NiTphI7jx')
  ],
  providers: [
    AuthentificationService,
    PhotoService,
    UtilisateurService,
    UploadFileService,
    HistoriqueUrlService,
    CommandeService,
    FormatService,
    VenteService,
    ThemesService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthentificationIntercepteur,
      multi: true
    },
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { displayDefaultIndicatorType: false }
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
