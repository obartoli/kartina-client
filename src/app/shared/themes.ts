import { Photo } from './photo';

export class Themes {
    themeId: number;
    description: string;
    theme: string;
    photographies: Photo[];
}
