import { Photo } from './photo';
import { Utilisateur } from './utilisateur';

export class Vente {
    venteId: number;
    dateDebut: string;
    dateFin: string;
    nombreExemplaire: number;
    nombreExemplaireVendu: number;
    photographie: Photo;
    utilisateur: Utilisateur;
}
