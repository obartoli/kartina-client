import {
  Finition
} from './finition';

export class Format {
  formatId: number;
  majoration: number;
  nomFormat: string;
  tailleFormat: string;
  finitions: Finition[];
}
