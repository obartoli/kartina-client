import { Cadre } from './cadre';
import { Finition } from './finition';
import { Format } from './format';
import { Vente } from './vente';

export class LigneCommande {
  ligneCommandeId: number;
  prixUnitaire: number;
  quantite: number;
  cadre: Cadre;
  finition: Finition;
  format: Format;
  vente: Vente;
}
