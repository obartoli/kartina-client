import { Vente } from './vente';

export class Utilisateur {
    utilisateurId: number;
    biographie: string;
    civilite: string;
    email: string;
    estBloque: string;
    motDePasse: string;
    nom: string;
    prenom: string;
    telephone: string;
    urlFb: string;
    urlPint: string;
    urlTwitter: string;
    adresses: any[];
    commandes: any[];
    ventes: Vente[];
    typeUtilisateur: string;
}