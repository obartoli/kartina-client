import { Cadre } from './cadre';
import { Format } from './format';

export class Finition {
  finitionId: number;
  descriptionFinition: string;
  majoration: number;
  nomFinition: string;
  cadres: Cadre[];
}
