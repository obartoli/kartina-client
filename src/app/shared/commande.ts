import { LigneCommande } from './ligne-commande';
import { StatutCommande } from './statut-commande';
import { Utilisateur } from './utilisateur';

export class Commande {
  commandeId: number;
  dateCommande: string;
  numeroFacture: string;
  prixTtc: number;
  statutCommande: StatutCommande;
  ligneCommandes: LigneCommande[];
  utilisateur: Utilisateur;
}
