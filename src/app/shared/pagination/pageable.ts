import { Sort } from './sort';

export class Pageable {
  content: any[];
  first: boolean;
  last: boolean;
  totalElements: number;
  totalPages: number;
  size: number;
  number: number;
  numberOfElements: number;
  empty: boolean;
  sort: Sort;
}
