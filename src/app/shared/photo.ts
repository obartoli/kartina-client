import { LigneCommande } from './ligne-commande';
import { Themes } from './themes';
import { Format } from './format';

export class Photo {
    photographieId: number;
    orientation: string;
    prixPhoto: number;
    titre: string;
    urlImage: string;
    urlMiniature: string;
    ligneCommandes: LigneCommande[];
    themes: Themes[];
    formats: Format[];
}
