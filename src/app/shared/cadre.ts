import { Finition } from './finition';

export class Cadre {
  cadreId: number;
  descriptionCadre: string;
  majorationCadre: number;
  nomCadre: string;
  finitions: Finition[];
}
