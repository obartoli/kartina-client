import {
  Injectable
} from '@angular/core';

import {
  HttpClient
} from '@angular/common/http';

import {
  shareReplay,
  tap
} from 'rxjs/operators';

import * as moment from 'moment';

import * as jwt_decode from 'jwt-decode';

@Injectable()
export class AuthentificationService {

  urlAuth: string = 'http://localhost:8080/authenticate'

  constructor(private http: HttpClient) {

  }

  login(login: string, password: string) {
    return this.http.post(this.urlAuth, {
        'username': login,
        'password': password
      })
      .pipe(
        tap(this.setSession),
        shareReplay()
      );
  }

  logout() {
    localStorage.removeItem("token");
    localStorage.removeItem("expires_at");
  }

  public isLoggedIn() {
    return moment().isBefore(this.getExpiration());
  }

  public getSubject() {
    let token = localStorage.getItem("token");
    if(token) {
      const decoded = jwt_decode(token);
      return decoded.sub;
    }
  }

  public getPrenom() {
    let token = localStorage.getItem("token");
    if(token) {
      const decoded = jwt_decode(token);
      return decoded.pre;
    }
  }

  public getAudience() {
    let token = localStorage.getItem("token");
    if(token) {
      const decoded = jwt_decode(token);
      return decoded.aud;
    }
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

  getExpiration() {
    const expiresAt = parseInt(localStorage.getItem("expires_at"));
    return moment.unix(expiresAt);
  }

  private setSession(authResult) {
    const decoded = jwt_decode(authResult.token);
    
    localStorage.setItem('token', authResult.token);
    localStorage.setItem('expires_at', JSON.stringify(decoded.exp));
  }
}
