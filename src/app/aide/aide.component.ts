import { Component, OnInit } from '@angular/core';
import { Utilisateur } from '../shared/utilisateur';

@Component({
  selector: 'app-aide',
  templateUrl: './aide.component.html',
  styleUrls: ['./aide.component.scss']
})
export class AideComponent implements OnInit {
  private u: Utilisateur = new Utilisateur();
  private sujets: string[] = ["Aide à la commande", "Service après vente", "Problème technique", "Vendre photos"];
  private suj: string = "Demande d'aide";
  private message: string;


  constructor() { }

  ngOnInit() {
  }

  sujetSelect(sujet: string) {
    this.suj = sujet;
  }

  envoiMessage() {
    this.message = (document.getElementById("message") as HTMLInputElement).value;
    console.log(this.u.civilite, this.u.prenom, this.u.nom, this.u.email, this.u.telephone, this.suj, this.message);
    if (this.message != null) {
      alert("Message bien envoyé!");
    }
    else { alert("Erreur lors de l'envoi du message!") }
  }

}
