import { Component, OnInit } from '@angular/core';
import { AuthentificationService } from '../authentification/authentification.service';
import { Commande } from '../shared/commande';
import { LigneCommande } from '../shared/ligne-commande';


@Component({
  selector: 'app-entete',
  templateUrl: './entete.component.html',
  styleUrls: ['./entete.component.scss']
})
export class EnteteComponent implements OnInit {
  
  get prenom(): string {
    return this.authService.getPrenom();
  };
  
  get estConnecte(): boolean {
    return this.authService.isLoggedIn();
  };

  constructor(private authService: AuthentificationService) { }

  ngOnInit() { }

}
