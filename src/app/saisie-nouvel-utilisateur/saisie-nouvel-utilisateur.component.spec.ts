import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaisieNouvelUtilisateurComponent } from './saisie-nouvel-utilisateur.component';

describe('SaisieNouvelUtilisateurComponent', () => {
  let component: SaisieNouvelUtilisateurComponent;
  let fixture: ComponentFixture<SaisieNouvelUtilisateurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaisieNouvelUtilisateurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaisieNouvelUtilisateurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
