import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

import {
  Utilisateur
} from '../shared/utilisateur';

import {
  UtilisateurService
} from '../service/utilisateur.service';

import {
  Router
} from '@angular/router';

import {
  Adresse
} from '../shared/adresse';

import {
  AuthentificationService
} from '../authentification/authentification.service';

import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-saisie-nouvel-utilisateur',
  templateUrl: './saisie-nouvel-utilisateur.component.html',
  styleUrls: ['./saisie-nouvel-utilisateur.component.scss']
})
export class SaisieNouvelUtilisateurComponent implements OnInit {

  @Input() parent: string;
  @Output() onUserChange = new EventEmitter < Utilisateur > ()

  private utilisateur = new Utilisateur();
  private adresse = new Adresse();
  private confirmationMotDePasse: string;
  private isEqual: boolean = true;

  constructor(private utilisateurService: UtilisateurService, private router: Router,
    private authentificationService: AuthentificationService) {
    let email = this.authentificationService.getSubject();
    if (email) {
      this.utilisateurService.getMonCompte(email).subscribe(response => {
        this.utilisateur = response as Utilisateur;
        if(this.utilisateur.adresses.length > 0) {
          this.adresse = this.utilisateur.adresses[0];
        } else {
          this.utilisateur.adresses = new Array <Adresse>();
          this.utilisateur.adresses.push(this.adresse);
        }
        console.log("emit", this.utilisateur);
        this.onUserChange.emit(this.utilisateur);
      });
    } else {
      this.utilisateur.adresses = new Array <Adresse>();
      this.utilisateur.adresses.push(this.adresse);
    }
  }

  ngOnInit() {}

  ajoutUtilisateur() {
    if (this.checkPassword() && this.utilisateur.email) {
      this.utilisateur.motDePasse = CryptoJS.SHA1(this.utilisateur.motDePasse.trim()).toString();
      this.utilisateurService.inscription(this.utilisateur).subscribe(
        () => {
          this.authentificationService.login(this.utilisateur.email, this.utilisateur.motDePasse).subscribe(() => {
            if(this.parent != 'paiement') {
              this.router.navigateByUrl('/');
            } else {
              this.onUserChange.emit(this.utilisateur);
            }
          });
        }
      );
    }
  }

  updateUtilisateur() {

    this.utilisateurService.update(this.utilisateur).subscribe(
      () => {
        
        this.onUserChange.emit(this.utilisateur);

        if(this.parent != 'paiement')
          this.router.navigateByUrl('/');
      }
    );
  }

  checkPassword(): boolean {
    if ((this.utilisateur.motDePasse == this.confirmationMotDePasse) &&
      (this.utilisateur.motDePasse && this.confirmationMotDePasse)) {
      this.isEqual = true;
    } else {
      this.isEqual = false;
    }
    return this.isEqual;
  }
}
