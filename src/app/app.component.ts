import {
  Component
} from '@angular/core';
import {
  HistoriqueUrlService
} from './service/historique-url.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'kartina-client';

  constructor(private urlService: HistoriqueUrlService) {}
}
