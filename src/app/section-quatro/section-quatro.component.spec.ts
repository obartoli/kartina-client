import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionQuatroComponent } from './section-quatro.component';

describe('SectionQuatroComponent', () => {
  let component: SectionQuatroComponent;
  let fixture: ComponentFixture<SectionQuatroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionQuatroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionQuatroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
