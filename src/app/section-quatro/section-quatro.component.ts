import { Component, OnInit, Input } from '@angular/core';
import { Vente } from '../shared/vente';

@Component({
  selector: 'app-section-quatro',
  templateUrl: './section-quatro.component.html',
  styleUrls: ['./section-quatro.component.scss']
})
export class SectionQuatroComponent implements OnInit {
  
  @Input() vente: Vente;
  @Input() titre: string;
  @Input() lien: string;
  @Input() descLien: string;
  
  constructor() { }

  ngOnInit() {
  }

}
