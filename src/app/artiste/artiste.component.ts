import { Component, OnInit, Input } from '@angular/core';
import { Utilisateur } from '../shared/utilisateur';
import { UtilisateurService } from '../service/utilisateur.service';
import { PhotoService } from '../service/photo.service';
import { Photo } from '../shared/photo';
import { Pageable } from '../shared/pagination/pageable';
import { Vente } from '../shared/vente';

@Component({
  selector: 'app-artiste',
  templateUrl: './artiste.component.html',
  styleUrls: ['./artiste.component.scss']
})
export class ArtisteComponent implements OnInit {
  @Input() lstArtiste: Array<Utilisateur>;
  lstVente: Pageable;
  utilisateurId: number;
  private randomUserSell: Vente;
  private display: boolean;

  constructor(private artisteServ: UtilisateurService, private photoServ: PhotoService) { 
    this.artisteServ.getArtiste().subscribe(response => {
      this.lstArtiste = response as Utilisateur[];
      console.log("user", this.artisteServ);
    });
    /*this.lstArtiste.forEach(element => {
      this.photoServ.getRandomUserVente(element.utilisateurId).subscribe(response => {
        this.randomUserSell = response as Vente;
        this.randomUserSell.utilisateur = this.element;
    });
    
    });*/
  }
  
  ngOnInit() {
  }

  apparition(){
    this.display=true;
  }
}
