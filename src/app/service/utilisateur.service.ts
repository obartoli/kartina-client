import {
  Injectable
} from '@angular/core';

import {
  HttpClient
} from '@angular/common/http';

import {
  Utilisateur
} from '../shared/utilisateur';

@Injectable({
  providedIn: 'root'
})

export class UtilisateurService {
  private _postUrl = "http://localhost:8080/api/user";
  private artisteURL = "http://localhost:8080/api/typeutilisateur";
  private userByIdURL = "http://localhost:8080/api/user/";
  private userByEmailURL = "http://localhost:8080/api/moncompte/";

  public lstUtilisateur: Array<Utilisateur> = new Array<Utilisateur>();
  public lstArtiste: Array<Utilisateur> = new Array<Utilisateur>();

  constructor(private _http: HttpClient) { }

  inscription(util: Utilisateur) {
    return this._http.post<Utilisateur>(this._postUrl, util);
  }

  update(util: Utilisateur) {
    console.log("update utilisateur : ", util);
    
    return this._http.put<Utilisateur>(this._postUrl, util);
  }

  getArtiste()
  {
    return this._http.get(this.artisteURL);
  }

  getUser(id: number) {
    let url = this.userByIdURL + id;
    console.log("ID", id);
    console.log(url);
    return this._http.get(url);
  }

  getMonCompte(email: String) {
    let url = this.userByEmailURL + email;
    console.log("email du connecté", email);
    console.log(url);
    return this._http.get(url);
  }
}