import { Injectable } from '@angular/core';
import {HttpClient, HttpRequest, HttpEvent} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Photo } from '../shared/photo';

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {
  private ajoutURL = "http://localhost:8080/api/post";

  constructor(private http: HttpClient) {}
 
  pushFileToStorage(file: File): Observable<Photo> {
    let formdata: FormData = new FormData();
 
    formdata.append('file', file);
 
    /*const req = new HttpRequest('POST', 'http://localhost:8080/api/post', formdata, {
      reportProgress: true,
      responseType: 'text'
    });
 
    return this.http.request(req);*/
    return this.http.post<Photo>(this.ajoutURL, formdata);
  }
}
