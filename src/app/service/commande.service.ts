import {
  Injectable
} from '@angular/core';
import {
  HttpClient
} from '@angular/common/http';
import {
  Commande
} from '../shared/commande';
import {
  Observable
} from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommandeService {

  //Insertion d'une nouvelle commande en base
  private newOrder: string = "http://localhost:8080/api/order";

  constructor(private http: HttpClient) {}

  public sendOrder(commande: Commande): Observable < Commande > {
    return this.http.post < Commande > (this.newOrder, commande);
  }

}
