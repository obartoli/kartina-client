import { Injectable } from '@angular/core';
import { Themes } from '../shared/themes';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThemesService {

  private getThemesUrl: string = "http://localhost:8080/api/theme/names";
  private getAllUrl: string = "http://localhost:8080/api/getthemes";

  public lstThemes: Array<Themes> = new Array<Themes>();

  constructor(private _http: HttpClient) { }

  public getTheme() : Observable <Array <string> >
  {
    return this._http.get<Array <string> >(this.getThemesUrl);
  }

  public getAll() {
    return this._http.get(this.getAllUrl);
  }
  
}
