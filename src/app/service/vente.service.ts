import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class VenteService {
  private _getUrl = "http://localhost:8080/api/sell/{id}";
  constructor(private _http: HttpClient) { }

  getVente(id: number) {
    let url= this._getUrl.replace("{id}", ''+id);
    return this._http.get(url);
  }
}
