import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { Format } from '../shared/format';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FormatService {

  //récupère une liste des noms de formats disponibles
  private getFormatsUrl: string = "http://localhost:8080/api/format/names";
  private allFormatURL: string = "http://localhost:8080/api/getformat";
  
  public lstFormats: Array<Format> = new Array<Format>();

  constructor(private http: HttpClient) { }

  public getFormats() : Observable <Array <string> >
  {
    return this.http.get<Array <string> >(this.getFormatsUrl);
  }

  public getAllFormat()
  {
    return this.http.get(this.allFormatURL);
  }
}
