import {
  Injectable
} from '@angular/core';
import {
  Router,
  NavigationEnd
} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class HistoriqueUrlService {

  private previousUrl: string;
  private currentUrl: string;

  constructor(private router: Router) {

    this.currentUrl = this.router.url;

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {        
        this.previousUrl = this.currentUrl;
        this.currentUrl = event.url;
        console.log("event route : ", this.previousUrl);
        console.log("event route : ", this.currentUrl);
      };
    })
  }

  public getPreviousUrl() : string {
    return this.previousUrl;
  }
}
