import { Injectable, Input } from '@angular/core';
import { Photo } from '../shared/photo';
import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pageable } from '../shared/pagination/pageable';
import { Vente } from '../shared/vente';
import { Upload } from '../shared/upload';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  // WebService pour récupérer les ventes en cours
  private allVenteURL: string           = "http://localhost:8080/api/sell/current";
  // WebService pour récupérer les 6 meilleurs ventes
  private bestVenteURL: string          = "http://localhost:8080/api/bestvente";
  // WebService pour récupérer les éditions limitées
  private lastCopiesURL: string         = "http://localhost:8080/api/sell/lastcopies/"
  // WebService pour récupérer les éditions limitées
  private newVenteURL: string           = "http://localhost:8080/api/newvente"

  private getByThemeUrl: string         = "http://localhost:8080/api/getbythemes/";
  private venteByUserURL: string        = "http://localhost:8080/api/venteutilisateur/";
  
  private venteCoursURL = "http://localhost:8080/api/ventecours/";
  private nvleVenteURL = "http://localhost:8080/api/addvente";
  private ventePasseeURL = "http://localhost:8080/api/ventepassee/";

  //Ventes random pour les sections de la page d'accueil
  private randomNewVenteURL: string     = "http://localhost:8080/api/sell/new/random";
  private randomThemeVenteURL: string   = "http://localhost:8080/api/sell/theme/{id}/random";
  private randomArtisteVenteURL: string = "http://localhost:8080/api/sell/user/{id}/random";
  private randomLastCopiesURL: string   = "http://localhost:8080/api/sell/lastcopies/random";

  //toutes les orientations possibles d'une photo
  private getOrientationsUrl: string    = "http://localhost:8080/api/photo/orientations";

  @Input() photo: Photo;
  public lstPhoto: Array<Photo> = new Array<Photo>();

  constructor(private http: HttpClient) { }

  public getBestVente()
  {
    return this.http.get(this.bestVenteURL);
  }

  public getNewVente()
  {
    return this.http.get(this.newVenteURL);
  }

  public getRandomNewVente(): Observable<Vente>
  {
    return this.http.get<Vente>(this.randomNewVenteURL);
  }

  public getRandomThemeVente(id: number): Observable<Vente>
  {
    let url = this.randomThemeVenteURL.replace("{id}", '' + id);
    return this.http.get<Vente>(url);
  }

  public getRandomUserVente(id: number): Observable<Vente>
  {
    let url = this.randomArtisteVenteURL.replace("{id}", '' + id);
    return this.http.get<Vente>(url);
  }

  public getRandomLastCopies(): Observable<Vente>
  {
    return this.http.get<Vente>(this.randomLastCopiesURL);
  }

  public getAllVente(criteria?: string): Observable<Pageable>
  {
    
    console.log(criteria);
    
    return this.http.get<Pageable>(this.allVenteURL + criteria);
  }

  public getByTheme(id: number, page?: number) 
  {
    let urlPage = '' + id;
    if(page) {
      urlPage += '?page=' + (page-1);
    }
    return this.http.get(this.getByThemeUrl + urlPage);
  }

  public getVenteByArtiste(id: number, page?: number)
  {
    let url = this.venteByUserURL + id;
    console.log("ID vente by artiste", id);
    console.log("URL vente", url);
    if(page) {
      url += '?page=' + (page-1);
    }
    console.log("URL vente", url);
    return this.http.get(url);
  }

  public getVenteCours(id: number, page?: number)
  {
    let url = this.venteCoursURL + id;
    console.log("ID vente en cours by artiste", id);
    console.log("URL vente en cours", url);
    if(page) {
      url += '?page=' + (page-1);
    }
    console.log("URL vente en cours", url);
    return this.http.get(url);
  }

  public getVentePassee(id: number, page?: number)
  {
    let url = this.ventePasseeURL + id;
    console.log("ID vente passée by artiste", id);
    console.log("URL passée", url);
    if(page) {
      url += '?page=' + (page-1);
    }
    console.log("URL passée", url);
    return this.http.get(url);
  }

  public getOrientations(): Observable <Array<string>> {
    return this.http.get <Array<string>> (this.getOrientationsUrl);
  }

  public getLastCopies(page?: number): Observable <Pageable> {
    return this.http.get <Pageable>  (this.lastCopiesURL);
  }

  public nvleVente(vente: Vente) { 
    return this.http.post<Vente>(this.nvleVenteURL, vente);
  }

  pushFileToStorage(file: File): Observable<HttpEvent<{}>> {
    let formdata: FormData = new FormData();
 
    formdata.append('file', file);
 
    const req = new HttpRequest('POST', 'http://localhost:8080/post', formdata, {
      reportProgress: true,
      responseType: 'text'
    });
 
    return this.http.request(req);
  }
}