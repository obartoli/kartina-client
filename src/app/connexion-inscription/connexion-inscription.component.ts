import {
  Component,
  OnInit
} from '@angular/core';

import {
  Router
} from '@angular/router';

import {
  AuthentificationService
} from '../authentification/authentification.service';
import {
  Utilisateur
} from '../shared/utilisateur';
import {
  HttpErrorResponse
} from '@angular/common/http';
import {
  HistoriqueUrlService
} from '../service/historique-url.service';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-connexion-inscription',
  templateUrl: './connexion-inscription.component.html',
  styleUrls: ['./connexion-inscription.component.scss']
})
export class ConnexionInscriptionComponent implements OnInit {

  private utilisateur: Utilisateur = new Utilisateur;

  constructor(private authService: AuthentificationService,
              private router: Router,
              private urlService: HistoriqueUrlService) {}

  ngOnInit() {}

  connexion() {

    console.log("password : ", this.utilisateur.motDePasse);
    console.log("encrypted : ", CryptoJS.SHA1(this.utilisateur.motDePasse.trim()).toString());
    
    
    if (this.utilisateur.email && this.utilisateur.motDePasse) {
      this.authService.login(this.utilisateur.email, CryptoJS.SHA1(this.utilisateur.motDePasse.trim()).toString())
        .subscribe(
          data => {
            console.log("data", data);
            let url = this.urlService.getPreviousUrl();
            console.log(url);
            
            if(url == '/compte') {
              url = '/';
            }
            this.router.navigateByUrl(url);
          },
          error => {
            alert((error as HttpErrorResponse).error.message);
          }
        );
    }
  }
}
