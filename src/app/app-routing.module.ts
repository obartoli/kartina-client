import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MentionsLegalesComponent } from './mentions-legales/mentions-legales.component';
import { ConditionsGeneralesUtilisationComponent } from './conditions-generales-utilisation/conditions-generales-utilisation.component';
import { AccueilComponent } from './accueil/accueil.component';
import { ConnexionInscriptionComponent } from './connexion-inscription/connexion-inscription.component';
import { SaisieNouvelUtilisateurComponent } from './saisie-nouvel-utilisateur/saisie-nouvel-utilisateur.component';
import { ArtisteComponent } from './artiste/artiste.component';
import { ParcoursCatalogueComponent } from './parcours-catalogue/parcours-catalogue.component';
import { PageArtisteComponent } from './page-artiste/page-artiste.component';
import { PageUtilisateurComponent } from './page-utilisateur/page-utilisateur.component';
import { ParcoursAchatComponent } from './parcours-achat/parcours-achat.component';
import { PanierComponent } from './panier/panier.component';
import { BiographieComponent } from './biographie/biographie.component';
import { VenteCoursComponent } from './vente-cours/vente-cours.component';
import { NouvelleVenteComponent } from './nouvelle-vente/nouvelle-vente.component';
import { VentePasseeComponent } from './vente-passee/vente-passee.component';
import { PaiementComponent } from './paiement/paiement.component';
import { AideComponent } from './aide/aide.component';


const routes: Routes = [
  { path: 'accueil', component: AccueilComponent },
  { path: 'artiste', component: ArtisteComponent },
  { path: 'artiste/:id', component: PageArtisteComponent },
  { path: 'mentionslegales', component: MentionsLegalesComponent },
  { path: 'conditionsgeneralesdutilisation', component: ConditionsGeneralesUtilisationComponent },
  { path: 'compte', component: ConnexionInscriptionComponent },
  { path: 'moncompte', component: PageUtilisateurComponent },
  { path: 'saisienouvelutilisateur', component: SaisieNouvelUtilisateurComponent},
  { path: 'saisienouvellevente', component: NouvelleVenteComponent},
  { path: 'editerbiographie', component: BiographieComponent},
  { path: 'venteencours/:id', component: VenteCoursComponent},
  { path: 'ventepassee/:id', component: VentePasseeComponent},
  { path: 'catalogue', component : ParcoursCatalogueComponent},
  { path: 'catalogue/:theme', component : ParcoursCatalogueComponent},
  { path: 'parcours/:id', component: ParcoursAchatComponent},
  { path: 'panier', component: PanierComponent},
  { path: 'paiement', component: PaiementComponent},
  { path: 'aide', component: AideComponent},
  { path: '', redirectTo: 'accueil', pathMatch: 'full' },
  { path: '**', redirectTo: 'accueil', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
